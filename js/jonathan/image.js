var Image = function(fileStream){

	//ce qui est ici constitue le contructeur de image
	
	this.$el = $('<div></div>');
	
	var $img = $('<img/>').appendTo(this.$el);
	
	$img.attr('src', fileStream);
	
	var $close = this.$el.append("<div><img src='close.png' /></div>");
	
	var that = this;
	$close.click(function(){
		Collection.remove(that);
	});

}