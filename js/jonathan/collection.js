var Collection = {
	
	//d�signe l'�l�ment DOM
	$el: $('#list'),
	
	//d�signe la collection d'images
	value: [],
	
	//permet d'ajouter une image � la collection
	add: function(image){

		this.value.unshift(image);
		this.$el.append(image.$el);
	},
	
	//permet de supprimer une image de la collection
	remove: function(image){

		this.value.splice(_.indexOf(this.value, image), 1);
		image.$el.remove();
	},

}